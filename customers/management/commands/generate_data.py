from django.core.management.base import BaseCommand

from django.core.management import call_command
from faker import Faker

from customers.models import Customer

fake = Faker()


def create_random_customer():
    Customer.objects.create(
        first_name=fake.first_name(),
        last_name=fake.last_name(),
        birthday=fake.date(),
        occupation=fake.job(),
        city=fake.city(),
        secret_alias=fake.color_name(),
    )


class Command(BaseCommand):
    help = 'Clears the database and generates mockup data.'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument(
            '--ncustomers',
            nargs="?",
            type=int,
            default=100,
            help="specify number of generated customers"
        )

    def handle(self, *args, **options):
        self.stdout.write("Clearing DB ...")
        call_command("flush")
        for customer in range(options['ncustomers']):
            create_random_customer()
        self.stdout.write("Done!")
