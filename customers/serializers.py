from rest_framework import serializers

from .models import Customer, Reminder


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Customer
        fields = ['id', 'first_name', 'last_name', 'city', 'occupation', 'birthday', 'secret_alias']


class ReminderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reminder
        fields = ['id', 'customer', 'created_at']
