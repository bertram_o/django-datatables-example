# Create your views here.
from django.forms import Form, ModelMultipleChoiceField, CheckboxSelectMultiple, MultipleHiddenInput
from django.shortcuts import render, redirect
from django.views.generic import ListView
from rest_framework import viewsets

from customers.models import Customer, Reminder
from customers.serializers import CustomerSerializer, ReminderSerializer


class ReminderList(ListView):
    model = Reminder
    template_name = 'customers/reminders.html'


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer


class ReminderViewSet(viewsets.ModelViewSet):
    queryset = Reminder.objects.all()
    serializer_class = ReminderSerializer


class BulkReminderForm(Form):
    # make select options available in html with hidden inputs (MultipleHiddenInput)
    # make them initially disabled, in template use jquery/datatables so set to disabled=false if selected
    customers = ModelMultipleChoiceField(
        widget=MultipleHiddenInput(attrs={'disabled': "true"}),
        label="customers",
        queryset=Customer.objects.all(),
    )


def bulk_create_reminder_for_customer(request):
    """
    Function based view handling the form.
    :param request:
    :return:
    """
    form = BulkReminderForm(
        request.POST or None,
        # needs to be set for MultipleHiddenInput! otherwise no input elements are in the generated html
        initial={'customers': Customer.objects.values_list('id', flat=True)}
    )
    if request.method == 'POST':
        if form.is_valid():
            reminders = []
            for customer in form.cleaned_data['customers']:
                reminders.append(Reminder.objects.create(customer=customer))
                print(f"Created reminder for customer {customer}")
            return redirect('reminders')

    return render(request, 'customers/reminder_create_form.html', {'form': form})
