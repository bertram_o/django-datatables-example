from django.db import models


class Customer(models.Model):
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128)
    city = models.CharField(max_length=128)
    occupation = models.CharField(max_length=128)
    birthday = models.DateField()
    secret_alias = models.CharField(max_length=256)

    def __repr__(self):
        return f"{self.first_name} {self.last_name} (ID {self.id})"


class Reminder(models.Model):
    created_at = models.DateTimeField(auto_now=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, default=False, blank=False)
