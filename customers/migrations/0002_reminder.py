# Generated by Django 3.2.7 on 2021-09-16 12:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reminder',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('customer', models.ForeignKey(default=False, on_delete=django.db.models.deletion.CASCADE, to='customers.customer')),
            ],
        ),
    ]
