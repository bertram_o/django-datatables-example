from django.urls import path, include
from rest_framework import routers

from customers.views import CustomerViewSet, bulk_create_reminder_for_customer, ReminderList, ReminderViewSet

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'customers', CustomerViewSet)
router.register(r'reminders', ReminderViewSet)

urlpatterns = [
    path('reminders', ReminderList.as_view(), name='reminders'),
    path('reminders/create', bulk_create_reminder_for_customer, name="create-reminders"),
    path('api/', include(router.urls))
]
